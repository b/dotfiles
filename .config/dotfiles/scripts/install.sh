#!/bin/bash

set -e

export DEBIAN_FRONTEND=noninteractive
_PACKAGE_MANAGER=""

if which pacman >/dev/null 2>&1; then
	_PACKAGE_MANAGER="pacman"
elif which apt >/dev/null 2>&1; then
	_PACKAGE_MANAGER="apt"
else
	echo "Couldn't find any recognised package managers, exiting."
	exit 1
fi

if ! which git >/dev/null 2>&1; then
	echo "Error: git is not installed on the system, it is required for further steps."
	exit 1
fi

# Install further package managers
if [ "$_PACKAGE_MANAGER" = "pacman" ]; then
	echo "Installing paru package manager"
	git clone https://aur.archlinux.org/paru.git
	cd paru
	makepkg -si
	cd ..
	rm -rf paru
elif [ "$_PACKAGE_MANAGER" = "apt" ]; then
	echo "Installing makedeb"
	export MAKEDEB_RELEASE='makedeb'
	bash -c "$(wget -qO - 'https://shlink.makedeb.org/install')"
fi

# Install nvim
echo "Installing neovim and dependencies"
if [ "$_PACKAGE_MANAGER" = "pacman" ]; then
	paru -S neovim cmake --noconfirm
elif [ "$_PACKAGE_MANAGER" = "apt" ]; then
	sudo apt -yq install cmake
	git clone https://mpr.makedeb.org/neovim
	cd neovim
	yes | makedeb -si
	cd ..
	rm -rf neovim
fi

# Install tmux
echo "Installing tmux"
if [ "$_PACKAGE_MANAGER" = "pacman" ]; then
	paru -S tmux --noconfirm
elif [ "$_PACKAGE_MANAGER" = "apt" ]; then
	sudo apt -yq install tmux
fi
