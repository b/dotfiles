# Clones this repo and sets up the "config" dotfiles alias
# This file is useful to quickly setup the dotfiles by calling the script through curl :)

set -e

if ! which git >/dev/null 2>&1; then
	echo "Error: git is not installed on the system, it is required for further steps."
	exit 1
fi

git clone --bare https://gitlab.com/b/dotfiles.git $HOME/.dotfiles

echo "alias config='/usr/bin/git --git-dir=\"\$HOME/.dotfiles/\" --work-tree=\"\$HOME\"'" >>~/.bashrc
if which zsh >/dev/null 2>&1; then
	echo "alias config='/usr/bin/git --git-dir=\"\$HOME/.dotfiles/\" --work-tree=\"\$HOME\"'" >>~/.zshrc
fi

source ~/.bashrc
config checkout -f
config config --local status.showUntrackedFiles no

echo "Cloning complete! :D - Run ~/.config/dotfiles/scripts/install.sh to install required programs (debian and arch only)"
